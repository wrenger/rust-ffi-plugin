#ifndef API_H
#define API_H

#include <inttypes.h>

/**
 * Version number of a plugin.
 * Version numbers match if the major is equal and minor and patch are greater
 * equal.
 */
struct PluginVersion {
    uint16_t major;
    uint16_t minor;
    uint16_t patch;
};

/**
 * Version number of an API.
 * Version numbers match if major is equal and minor is greater equal.
 */
struct PluginAPIVersion {
    uint16_t major;
    uint16_t minor;
};

/**
 * An API defines the functionality a plugin provides.
 * A plugin may however define multiple APIs.
 * Also APIs may depend on other APIs.
 */
struct PluginAPI {
    uint64_t uuid;
    char const *name;
    struct PluginAPIVersion version;
    // zero-terminated list of api names.
    char const *const *dependencies;
    /**
     * @brief The specified api is initialized.
     * It has to store its dependencies on its own if they are needed during
     * runtime
     *
     * @param api The api to initialize
     * @param dependencies Zero-terminated list of already initialized APIs
     */
    void (*init)(struct PluginAPI *self, struct PluginAPI **dependencies);
    /**
     * @brief The api is unloaded and has to free its resources.
     * The api is not used again after this call.
     */
    void (*uninit)(struct PluginAPI *self);

    // ... additional data & function pointers
};

/**
 * @brief Returns the name of the plugin.
 */
const char *plugin_name();
typedef const char *(*plugin_name_ptr)();

/**
 * @brief Returns the version of this plugin.
 */
struct PluginVersion plugin_version();
typedef struct PluginVersion (*plugin_version_ptr)();

/**
 * @brief Returns a zero-terminated list of plugins that are implemented by this
 * plugin
 */
const struct PluginAPI **plugin_provides();
typedef struct PluginAPI **(*plugin_provides_ptr)();

#endif
