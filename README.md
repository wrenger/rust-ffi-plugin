# Rust FFI Plugin Template

This is an example project for plugin management using dynamic libraries in Rust.
It uses the C FFI and supports plugins written in C/C++ and Rust.

## Project Structure

The [src](src) contains the plugin host. It loads plugins as dynamic libraries and
provides an interface with functions the plugins can call.

This interface is defined in [src/interface.rs](src/interface.rs).
It contains all functions that are exposed and can be called from the plugin.

Currently there are two example plugins:
One written in C, located at [plugin-c](plugin-c) and the other one written in
rust at [plugin-rs](plugin-rs).

Each of the plugins have to specify the functions declared in [plugin-c/plugin.h](plugin-c/plugin.h).
To add new functions the [`Plugin`](src/main.rs) struct from the host and the plugins have to be extended accordingly.

## Build & Run

To run the example first both plugins have to be build:

**C Plugin**
```
cd <project/root>/plugin-c
make
```

**Rust Plugin**
```
cd <project/root>/plugin-rs
cargo build
```

Then the plugin host can be build and run:
```
cd <project/root>
cargo run
```
