use std::ptr;
use std::os::raw::c_char;

mod api;
use api::*;

static algorithm_api_dependencies: [*const c_char; 2] = [
    "example-c\0".as_ptr() as *const c_char,
    ptr::null(),
];

#[repr(C)]
pub struct AlgorithmAPI {
    pub base: PluginAPI,
    sum: unsafe extern "C" fn(*mut AlgorithmAPI, i32) -> i32,
}

static algorithm_api_name: &[u8; 13] = b"AlgorithmAPI\0";

static algorithm_api: AlgorithmAPI = AlgorithmAPI {
    base: PluginAPI {
        name: algorithm_api_name.as_ptr() as *const c_char,
        uuid: 2,
        version: PluginAPIVersion { major: 1, minor: 1 },
        dependencies: algorithm_api_dependencies.as_ptr(),
        init: Some(init),
        uninit: Some(uninit),
    },
    sum: sum,
};

pub extern "C" fn init(api: *mut PluginAPI, dependencies: *mut *mut PluginAPI) {

}

pub extern "C" fn uninit(api: *mut PluginAPI) {

}

pub extern "C" fn sum(api: *mut AlgorithmAPI, n: i32) -> i32 {
    let mut result = 0;
    for i in 1..n {
        result += i;
    }
    return result;
}


#[no_mangle]
pub extern "C" fn plugin_name() -> *const c_char {
    b"example-rs\0".as_ptr() as *const c_char
}

#[no_mangle]
pub extern "C" fn plugin_version() -> PluginVersion {
    PluginVersion { major: 1, minor: 0, patch: 0 }
}

#[no_mangle]
pub extern "C" fn plugin_provides() ->  *mut *mut PluginAPI {
    ptr::null_mut()
}
