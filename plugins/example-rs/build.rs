use std::env;
use std::path::PathBuf;

const BINDINGS_HEADERS: [&str; 2] = [
    "../../include/api.h",
    "../example-c/plugin.h",
];
const INCLUDE_DIRS: [&str; 2] = [
    "../../include/",
    "../example-c/",
];
const BINDINGS_OUT: &str = "bindings.rs";

fn main() {
    let project_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    let include_args = INCLUDE_DIRS.iter().map(|dir| {
        format!("-I{}", &[project_dir.as_str(), dir].join("/"))
    }).collect::<Vec<_>>();

    let mut builder = bindgen::builder()
        .whitelist_function("plugin_.*")
        .whitelist_type("Plugin.*")
        .whitelist_var("plugin_.*")
        .clang_args(&include_args);

    for &header in &BINDINGS_HEADERS {
        builder = builder.header(header);
    }

    let bindings = builder.generate().expect("Bindings generation failed");

    bindings
        .write_to_file(out_dir.join(BINDINGS_OUT))
        .expect("Bindings saving failed");
}
