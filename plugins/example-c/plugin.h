#ifndef PLUGIN_C
#define PLUGIN_C

#include "api.h"
#include <inttypes.h>

struct ArithmeticsAPI {
    struct PluginAPI base;
    int32_t (*add)(struct ArithmeticsAPI *self, int32_t a, int32_t b);
    int32_t (*sub)(struct ArithmeticsAPI *self, int32_t a, int32_t b);
    int32_t (*mul)(struct ArithmeticsAPI *self, int32_t a, int32_t b);
};

#endif
