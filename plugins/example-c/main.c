#include "plugin.h"

#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>

// ----------------------------------------------------------------------------
// Implementing the ArithmeticsAPI.
// ----------------------------------------------------------------------------

static void init(struct PluginAPI *self, struct PluginAPI **dependencies) {
    printf("Init %s (%d, %d)\n", self->name, self->version.major,
           self->version.minor);
}

static void uninit(struct PluginAPI *self) {
    printf("Uninit %s (%d, %d)\n", self->name, self->version.major,
           self->version.minor);
}

static int32_t add(struct ArithmeticsAPI *api, int32_t a, int32_t b) {
    return a + b;
}

static int32_t sub(struct ArithmeticsAPI *api, int32_t a, int32_t b) {
    return a - b;
}

static int32_t mul(struct ArithmeticsAPI *api, int32_t a, int32_t b) {
    return a * b;
}

struct ArithmeticsAPI arithmetics_api = {
    {
        .version = {1, 0},
        .name = "ArithmeticsAPI",
        .init = init,
        .uninit = uninit,
    },
    .add = add,
    .sub = sub,
    .mul = mul,
};

// ----------------------------------------------------------------------------
// Required plugin functions.
// ----------------------------------------------------------------------------

struct PluginAPI *provided_apis[] = {
    &arithmetics_api.base,
    NULL,
};

const char *plugin_name() { return "example-c"; }

struct PluginVersion plugin_version() {
    return (struct PluginVersion){1, 0, 0};
}

const struct PluginAPI **plugin_provides() {
    return provided_apis;
}
