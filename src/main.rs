use std::ffi::CStr;
use std::io;
use std::os::raw;
use std::path::{Path, PathBuf};
use std::ptr;

#[macro_use]
extern crate dlopen_derive;

use dlopen::wrapper::{Container, WrapperApi};

mod api;

/// Safe abstraction for loaded plugins
pub struct Plugin {
    /// Keeping the library so that our symbols doesn't become invalid.
    _lib: Container<PluginWrapper>,
    // Config
    pub name: String,
    pub version: api::PluginVersion,
    pub provides: &'static [&'static mut api::PluginAPI],
}

#[derive(WrapperApi)]
pub struct PluginWrapper {
    plugin_name: unsafe extern "C" fn() -> *const raw::c_char,
    plugin_version: unsafe extern "C" fn() -> api::PluginVersion,
    plugin_provides: unsafe extern "C" fn() -> *mut *mut api::PluginAPI,
}

impl Plugin {
    /// Loads the plugin from the given file `path`
    pub fn load<P: AsRef<Path>>(path: P) -> io::Result<Plugin> {
        println!("Opening {:?}", path.as_ref());
        let plugin = unsafe {
            let lib = Container::<PluginWrapper>::load(path.as_ref())
                .map_err(|e| io::Error::new(io::ErrorKind::Other, e.to_string()))?;
            // Load and initialize plugin config
            let name = lib.plugin_name();
            let name = CStr::from_ptr(name).to_string_lossy().into_owned();
            println!("Name {:?}", name);
            let version = lib.plugin_version();
            let provides = lib.plugin_provides();

            let provides: &[&mut api::PluginAPI] = {
                let mut len = 0;
                if !provides.is_null() {
                    while !(*(provides.add(len))).is_null() && len <= 100 {
                        len += 1;
                    }
                    if len >= 100 {
                        return Err(io::Error::new(
                            io::ErrorKind::Other,
                            "Provides buffer not terminated",
                        ));
                    }
                }
                ptr::slice_from_raw_parts(provides as *const &mut api::PluginAPI, len)
                    .as_ref()
                    .unwrap_or_default()
            };
            println!("Provides {:?}", provides);

            Plugin {
                _lib: lib,
                name,
                version,
                provides,
            }
        };
        println!(
            "Loaded plugin: {:?} version: {:?}",
            plugin.name, plugin.version
        );
        Ok(plugin)
    }
}

fn main() {
    let project_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    let plugin_c = project_dir.join("plugins/example-c/main.so");
    let plugin_rs = project_dir.join("plugins/example-rs/target/debug/libexample_rs.so");

    let mut plugins = vec![];
    plugins.push(Plugin::load(plugin_c).expect("Unable to load plugin"));
    plugins.push(Plugin::load(plugin_rs).expect("Unable to load plugin"));

    for plugin in &plugins {
        println!("Plugin {} loaded", plugin.name);
    }
}
